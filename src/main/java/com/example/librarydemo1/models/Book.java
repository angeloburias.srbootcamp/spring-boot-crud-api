package com.example.librarydemo1.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "tbl_books")
public class Book {

  public Book() {
  }

  public Book(String Code, String Title, String Desc, String Image) {
    this.code = Code;
    this.title = Title;
    this.description = Desc;
    this.thumbnail = Image;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "code")
  private String code;

  @Column(name = "title")
  private String title;

  @Column(name = "description")
  private String description;

  @Column(name = "thumbnail")
  private String thumbnail;

  @Column(name = "publisher_id")
  private Long publisher_id;

  @Column(name = "author_id")
  private Long author_id;

  @Column(name = "category_id")
  private Long category_id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getAuthorId() {
    return author_id;
  }

  public void setAuthorId(Long id) {
    this.author_id = id;
  }

  public Long getPublisherID() {
    return publisher_id;
  }

  public void setPublisherID(Long pubId) {
    this.publisher_id = pubId;
  }

  public void setCategoryID(Long categoryID) {
    this.category_id = categoryID;
  }

  public Long getCategoryID() {
    return category_id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String Title) {
    this.title = Title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String desc) {
    this.description = desc;
  }

  public String getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(String thum) {
    this.thumbnail = thum;
  }
}

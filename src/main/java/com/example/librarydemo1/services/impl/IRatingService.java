package com.example.librarydemo1.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.librarydemo1.models.Rating;
import com.example.librarydemo1.repositories.RatingRepository;
import com.example.librarydemo1.services.RatingService;

@Service
public class IRatingService implements RatingService {

  @Autowired
  private RatingRepository ratingRepository;

  @Override
  public Rating saveRating(Rating rating) {
    return ratingRepository.save(rating);
  }

  @Override
  public List<Rating> getRatings() {
    return ratingRepository.findAll();
  }

  @Override
  public Rating updateRating(Rating rating, Long ratingId) {
    Optional<Rating> currentRating = ratingRepository.findById(ratingId);

    Rating ra = currentRating.get();
    ra.setRating(ra.getRating());

    return ratingRepository.save(ra);
  }

  @Override
  public void deleteRatingById(Long ratingId) {
    ratingRepository.deleteById(ratingId);

  }

}

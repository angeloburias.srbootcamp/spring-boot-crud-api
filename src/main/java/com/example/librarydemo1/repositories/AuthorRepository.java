package com.example.librarydemo1.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.librarydemo1.models.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

  @Query(value = "SELECT tbl_books.title, tbl_authors.firstname FROM tbl_books INNER JOIN tbl_authors ON tbl_authors.author_id = tbl_books.author_id WHERE tbl_authors.author_id =?1", nativeQuery = true)
  Optional<?> getAuthorBooks(Long id);

}

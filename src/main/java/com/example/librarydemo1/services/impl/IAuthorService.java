package com.example.librarydemo1.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.librarydemo1.models.Author;
import com.example.librarydemo1.repositories.AuthorRepository;
import com.example.librarydemo1.services.AuthorService;

@Service
public class IAuthorService implements AuthorService {

  @Autowired
  private AuthorRepository authorRepository;

  @Override
  public void deleteAuthorById(Long authorId) {
    authorRepository.deleteById(authorId);
  }

  @Override
  public Author updateAuthor(Author author, Long authorId) {

    Optional<Author> currentAuthor = authorRepository.findById(authorId);

    Author aut = currentAuthor.get();
    aut.setFirstname(aut.getFirstname());
    aut.setLastname(aut.getLastname());

    return authorRepository.save(aut);
  }

  @Override
  public List<Author> getAuthorList() {
    return authorRepository.findAll();
  }

  @Override
  public Optional<?> getAuthorBooks(Long id) {
    return authorRepository.getAuthorBooks(id);
  }

  @Override
  public Author saveAuthor(Author author) {
    return authorRepository.save(author);
  }

}

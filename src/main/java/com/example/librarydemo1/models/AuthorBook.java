package com.example.librarydemo1.models;

// this class serve as the gateway for all the other classes
// by accessing this class, you can access multiple classes inside this authorbook class
public class AuthorBook {

  // Author class
  private Author author;

  // Book class
  private Book book;

  // Category class
  private Category category;

  // Publisher class
  private Publisher publisher;

  // Rating class
  private Rating rating;

  // Author Book constructor
  public AuthorBook() {

  }

  // Author Book constructor with parameters
  public AuthorBook(Author aut, Book book, Category category, Publisher pub, Rating rating) {
    this.author = aut;
    this.book = book;
    this.category = category;
    this.publisher = pub;
    this.rating = rating;
  }

  // returns Author class
  public Author getAuthor() {
    return author;
  }

  // pass the author class
  public void setAuthor(Author auth) {
    this.author = auth;
  }

  // return Book class
  public Book getBook() {
    return book;
  }

  // pass the Book class
  public void setBook(Book book) {
    this.book = book;
  }

  // return the Category class
  public Category getCategory() {
    return category;
  }

  // pass the Category class
  public void setCategory(Category category) {
    this.category = category;
  }

  // return the Publisher class
  public Publisher getPublisher() {
    return publisher;
  }

  // pass the Publisher class
  public void setPublisher(Publisher publisher) {
    this.publisher = publisher;
  }

  // return Rating class
  public Rating getRating() {
    return rating;
  }

  // pass the publisher class
  public void setRating(Rating rating) {
    this.rating = rating;
  }

}

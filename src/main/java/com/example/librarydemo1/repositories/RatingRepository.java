package com.example.librarydemo1.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.librarydemo1.models.Rating;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {

}
package com.example.librarydemo1.services;

import java.util.List;

import com.example.librarydemo1.models.AuthorBook;

public interface AuthorBookServices {

  AuthorBook saveAuthorBook(AuthorBook author);

  List<AuthorBook> getAuthorList();

  AuthorBook updateAuthorBook(AuthorBook author, Long authorId);

  void deleteAuthorById(Long employeeId);
}

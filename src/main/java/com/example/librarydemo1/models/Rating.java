package com.example.librarydemo1.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_ratings")
public class Rating {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "rating")
  private String rating;

  @Column(name = "book_id")
  private Long book_id;

  public Rating() {
  }

  public Rating(Long id, String Rating) {
    this.id = id;
    this.rating = Rating;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getBookID() {
    return book_id;
  }

  public void setBookID(Long BookID) {
    this.book_id = BookID;
  }

  public String getRating() {
    return rating;
  }

  public void setRating(String Rating) {
    this.rating = Rating;
  }
}

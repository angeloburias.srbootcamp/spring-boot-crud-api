package com.example.librarydemo1.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.librarydemo1.models.Author;
import com.example.librarydemo1.services.AuthorService;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class AuthorController {

  @Autowired
  public AuthorService authorService;

  public AuthorController() {

  }

  // save author
  @PostMapping("/saveAuthor")
  public ResponseEntity<?> createAuthor(@RequestBody Author author) {
    Author saveAuthor = authorService.saveAuthor(author);
    return new ResponseEntity<>(saveAuthor, HttpStatus.CREATED);
  }

  // get all authors
  @GetMapping("/authors")
  public ResponseEntity<List<Author>> getAllAuthors() {
    List<Author> author = authorService.getAuthorList();
    return new ResponseEntity<>(author, HttpStatus.OK);
  }

  // update author
  @PutMapping("/updateAuthor/{id}")
  public ResponseEntity<Author> updateAuthor(@PathVariable("id") Long id, @RequestBody Author author) {
    Author updateAuthorService = authorService.updateAuthor(author, id);
    return new ResponseEntity<>(updateAuthorService, HttpStatus.OK);
  }

  // delete an author
  @DeleteMapping("/author/{id}")
  public ResponseEntity<?> deleteEmployee(@PathVariable("id") Long id) {
    authorService.deleteAuthorById(id);
    return new ResponseEntity<>("ID Deleted Successfully: " + id, HttpStatus.OK);
  }
}

package com.example.librarydemo1.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.librarydemo1.models.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> {

  @Query(value = "SELECT tbl_books.title as BookTitle, tbl_publishers.name as PublisherName FROM tbl_books INNER JOIN tbl_publishers ON tbl_publishers.id = tbl_books.publisher_id WHERE tbl_publishers.id =?1", nativeQuery = true)
  Optional<?> getPublisherBooks(Long id);
}

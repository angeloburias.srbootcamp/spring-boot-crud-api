package com.example.librarydemo1.services;

import java.util.List;
import java.util.Optional;

import com.example.librarydemo1.models.Publisher;

public interface PublisherService {
  Publisher savePublisher(Publisher publisher);

  List<Publisher> getPublisherList();

  Optional<?> getPublisherBooks(Long id);

  Publisher updatePublisher(Publisher publisher, Long publisherId);

  void deletePublisherById(Long publisherId);
}
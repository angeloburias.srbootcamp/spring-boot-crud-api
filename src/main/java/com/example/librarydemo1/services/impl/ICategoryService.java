package com.example.librarydemo1.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.librarydemo1.models.Category;
import com.example.librarydemo1.repositories.CategoryRepository;
import com.example.librarydemo1.services.CategoryService;

@Service
public class ICategoryService implements CategoryService {

  @Autowired
  private CategoryRepository categoryRepository;

  @Override
  public Category saveCategory(Category category) {
    return categoryRepository.save(category);
  }

  @Override
  public List<Category> getCategoryList() {
    return categoryRepository.findAll();
  }

  @Override
  public Category updateCategory(Category category, Long id) {
    Optional<Category> currentBook = categoryRepository.findById(id);

    Category ct = currentBook.get();
    ct.setName(ct.getName());

    return categoryRepository.save(ct);
  }

  @Override
  public void deleteCategoryById(Long id) {
    categoryRepository.deleteById(id);
  }

}

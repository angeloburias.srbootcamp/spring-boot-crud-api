package com.example.librarydemo1.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.librarydemo1.models.Book;
import com.example.librarydemo1.repositories.BookRepository;
import com.example.librarydemo1.services.BookService;

@Service
public class IBookService implements BookService {

  @Autowired
  private BookRepository bookRepository;

  @Override
  public void deleteBookById(Long id) {
    bookRepository.deleteById(id);
  }

  @Override
  public Book updateBook(Book book, Long id) {

    Optional<Book> currentBook = bookRepository.findById(id);

    Book bk = currentBook.get();
    bk.setTitle(book.getTitle());
    bk.setCode(book.getCode());
    bk.setDescription(book.getDescription());

    return bookRepository.save(bk);
  }

  @Override
  public List<Book> getBookList() {
    return bookRepository.findAll();
  }

  @Override
  public Book saveBook(Book book) {
    return bookRepository.save(book);
  }

  @Override
  public Optional<Book> getBookByID(Long id) {
    return bookRepository.findById(id);
  }

}

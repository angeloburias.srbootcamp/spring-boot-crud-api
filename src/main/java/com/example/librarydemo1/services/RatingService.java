package com.example.librarydemo1.services;

import java.util.List;

import com.example.librarydemo1.models.Rating;

public interface RatingService {
  Rating saveRating(Rating rating);

  List<Rating> getRatings();

  Rating updateRating(Rating rating, Long ratingId);

  void deleteRatingById(Long ratingId);
}

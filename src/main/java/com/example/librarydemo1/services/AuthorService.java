package com.example.librarydemo1.services;

import java.util.List;
import java.util.Optional;

import com.example.librarydemo1.models.Author;

// serves as blueprints for object methods
public interface AuthorService {

  Author saveAuthor(Author author);

  List<Author> getAuthorList();

  Optional<?> getAuthorBooks(Long id);

  Author updateAuthor(Author author, Long authorId);

  void deleteAuthorById(Long employeeId);
}

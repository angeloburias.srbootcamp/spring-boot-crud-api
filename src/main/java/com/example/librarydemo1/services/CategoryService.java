package com.example.librarydemo1.services;

import java.util.List;

import com.example.librarydemo1.models.Category;

public interface CategoryService {

  Category saveCategory(Category category);

  List<Category> getCategoryList();

  Category updateCategory(Category category, Long id);

  void deleteCategoryById(Long id);
}

package com.example.librarydemo1.models;

import java.sql.Date;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "tbl_authors")
public class Author {

  @Id
  @Column(name = "author_id")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long author_id;

  @Column(name = "firstname")
  private String firstname;

  @Column(name = "middlename")
  private String middlename;

  @Column(name = "lastname")
  private String lastname;

  @Column(name = "timestamps")
  private Date timestamps;

  Date timeNow = Date.valueOf(LocalDate.now());

  public Author() {
  }

  public Author(String fname, String mname, String lname) {
    this.firstname = fname;
    this.middlename = mname;
    this.lastname = lname;
    this.timestamps = timeNow;
  }

  public Long getAuthorID() {
    return author_id;
  }

  public void setAuthorID(Long id) {
    this.author_id = id;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String name) {
    this.firstname = name;
  }

  public String getMiddlename() {
    return middlename;
  }

  public void setMiddlename(String mname) {
    this.middlename = mname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lname) {
    this.lastname = lname;
  }

  public Date getTimestamps() {
    return timestamps;
  }

  public void setTimestamps() {
    this.timestamps = timeNow;
  }
}

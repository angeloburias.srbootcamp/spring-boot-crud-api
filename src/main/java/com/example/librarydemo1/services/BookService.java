package com.example.librarydemo1.services;

import java.util.List;
import java.util.Optional;

import com.example.librarydemo1.models.Book;

// serves as blueprints for object methods
public interface BookService {

  Book saveBook(Book book);

  List<Book> getBookList();

  Optional<Book> getBookByID(Long id);

  Book updateBook(Book book, Long id);

  void deleteBookById(Long id);
}

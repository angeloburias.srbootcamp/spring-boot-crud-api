package com.example.librarydemo1.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.librarydemo1.models.Publisher;
import com.example.librarydemo1.repositories.PublisherRepository;
import com.example.librarydemo1.services.PublisherService;

@Service
public class IPublisherService implements PublisherService {

  @Autowired
  private PublisherRepository publisherRepository;

  @Override
  public Publisher savePublisher(Publisher publisher) {
    return publisherRepository.save(publisher);
  }

  @Override
  public List<Publisher> getPublisherList() {
    return publisherRepository.findAll();
  }

  @Override
  public Publisher updatePublisher(Publisher publisher, Long publisherId) {
    Optional<Publisher> currentPub = publisherRepository.findById(publisherId);

    Publisher pb = currentPub.get();
    pb.setName(pb.getName());
    pb.setLocation(pb.getLocation());
    return publisherRepository.save(pb);
  }

  @Override
  public void deletePublisherById(Long publisherId) {
    publisherRepository.deleteById(publisherId);
  }

  @Override
  public Optional<?> getPublisherBooks(Long id) {
    return publisherRepository.getPublisherBooks(id);
  }

}

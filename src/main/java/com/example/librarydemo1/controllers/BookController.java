package com.example.librarydemo1.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.librarydemo1.models.Author;
import com.example.librarydemo1.models.AuthorBook;
import com.example.librarydemo1.models.Book;
import com.example.librarydemo1.models.Category;
import com.example.librarydemo1.models.Publisher;
import com.example.librarydemo1.models.Rating;
import com.example.librarydemo1.services.AuthorService;
import com.example.librarydemo1.services.BookService;
import com.example.librarydemo1.services.CategoryService;
import com.example.librarydemo1.services.PublisherService;
import com.example.librarydemo1.services.RatingService;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class BookController {

  @Autowired
  public BookService bookService;

  @Autowired
  public AuthorService authorService;

  @Autowired
  public CategoryService categoryService;

  @Autowired
  public PublisherService publisherService;

  @Autowired
  public RatingService ratingService;

  @PostMapping("/saveBook")
  public ResponseEntity<?> createBook(@RequestBody AuthorBook book) {

    try {
      // save a book
      Book saveBook = bookService.saveBook(book.getBook());

      // save the publisher
      Publisher savePublisher = publisherService.savePublisher(book.getPublisher());

      // save the author
      // Author saveAuthor =
      Author auth = authorService.saveAuthor(book.getAuthor());

      // save category
      Category saveCategory = categoryService.saveCategory(book.getCategory());

      Rating rating = ratingService.saveRating(book.getRating());

      // pass the book id for rating table
      rating.setBookID(saveBook.getId());

      // pass the category id for category id field in the book table
      saveBook.setCategoryID(saveCategory.getId());

      // pass the publisher id for publisher id field in the book table
      saveBook.setPublisherID(savePublisher.getId());

      // pass the author id for author id field in the book table
      saveBook.setAuthorId(auth.getAuthorID());

      // save the rating of the book
      Rating saveRating = ratingService.saveRating(book.getRating());

      // pass the id for relational purposes
      saveRating.setBookID(saveBook.getId());
      // authorsBooksService.saveAuthorsBook(save)

      return new ResponseEntity<>(saveBook, HttpStatus.CREATED);

    } catch (Exception e) {
      e.getMessage(); // returns the error message IF there is any
    }
    return null; // null if not success
  }

  @GetMapping("/books")
  public ResponseEntity<List<Book>> getAllBooks() {
    List<Book> book = bookService.getBookList();
    return new ResponseEntity<>(book, HttpStatus.OK);
  }

  @PutMapping("/updateBook/{id}")
  public ResponseEntity<Book> updateAuthor(@PathVariable("id") Long id, @RequestBody Book book) {
    Book updateBookService = bookService.updateBook(book, id);
    return new ResponseEntity<>(updateBookService, HttpStatus.OK);
  }

  @PutMapping("/book/{id}")
  public ResponseEntity<?> getBook(@PathVariable("id") Long id) {
    Optional<?> book = bookService.getBookByID(id);
    return new ResponseEntity<>(book, HttpStatus.OK);
  }

  @PutMapping("/authorbooks/{id}")
  public ResponseEntity<?> getBooksByAuthor(@PathVariable("id") Long id) {
    try {
      Optional<?> book = authorService.getAuthorBooks(id);
      return new ResponseEntity<>(book, HttpStatus.OK);
    } catch (Exception e) {
      e.getMessage();
    }

    return null;
  }

  @PutMapping("/Publisherbooks/{id}")
  public ResponseEntity<?> getBooksByPublisher(@PathVariable("id") Long id) {
    Optional<?> books = publisherService.getPublisherBooks(id);
    return new ResponseEntity<>(books, HttpStatus.OK);
  }

  @DeleteMapping("/deleteBook/{id}")
  public ResponseEntity<?> deleteEmployee(@PathVariable("id") Long id) {
    bookService.deleteBookById(id);
    return new ResponseEntity<>("ID Deleted Successfully: " + id, HttpStatus.OK);
  }
}
